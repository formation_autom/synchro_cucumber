Feature: Connexion � l'application Jpetstore

  Scenario Outline: Connexion
    Given un navigateur est ouvert
    When je suis sur le site
    And je clique sur le lien de connexion
    And je rentre le username <username>
    And je rentre le password <pwd>
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil <msg> 
    @Adminprofil
    Examples: 
    |username | pwd | msg |
		|"j2ee"|"j2ee"|"Welcome ABC!"|
		|"ACID"|"ACID"|"Welcome ABC!"|
		