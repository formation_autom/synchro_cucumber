package jpet;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JPetSteps {
	
	WebDriver driver; 
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		
		System.setProperty("webdriver.chrome.driver", "C:/Users/Formation/AppData/Local/rasjani/WebDriverManager/bin/chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	  
	}

	@When("je suis sur le site")
	public void je_suis_sur_le_site() {
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		assertEquals("JPetStore Demo",driver.getTitle());
		
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
	    driver.findElement(By.xpath("//a[.=\"Sign In\"]")).click();
	    driver.findElement(By.xpath("//form/p[.=\"Please enter your username and password.\"]")).isDisplayed();
	}

	@When("je rentre le username {string}")
	public void je_rentre_le_username(String username) {
	    driver.findElement(By.name("username")).clear();
	    driver.findElement(By.name("username")).sendKeys(username);
	}

	@When("je rentre le password {string}")
	public void je_rentre_le_password(String pwd) {
		driver.findElement(By.name("password")).clear();
	    driver.findElement(By.name("password")).sendKeys(pwd);
	}

	@When("je clique sur login")
	public void je_clique_sur_login() {
	    driver.findElement(By.name("signon")).click();
	}

	@Then("utilisateur ABC est connecte")
	public void utilisateur_ABC_est_connecte() {
	    driver.findElement(By.xpath("//a[.='Sign Out']")).isDisplayed();
	}

	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil(String msg_accueil) {
		driver.findElement(By.id("WelcomeContent")).isDisplayed();
	    assertEquals(msg_accueil, driver.findElement(By.id("WelcomeContent")).getText());
	    driver.close();
	    driver.quit();
	}
	

}
