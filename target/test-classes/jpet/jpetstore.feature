Feature: Connexion � l'application Jpetstore

  Scenario: Connexion
    Given un navigateur est ouvert
    When je suis sur le site
    And je clique sur le lien de connexion
    And je rentre le username "j2ee"
    And je rentre le password "j2ee"
    And je clique sur login
    Then utilisateur ABC est connecte
    And je peux lire le message accueil "Welcome ABC!"

